package cr.ac.ucr.cql.miexamen01;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.app.FragmentTransaction;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView list;
    private List<String> itemname;
    private List<Integer> imgId;
    private List<String> itemdescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DeploymentScript.Run(getApplicationContext());

        itemname = new ArrayList<>();
        imgId = new ArrayList<>();
        itemdescription = new ArrayList<>();

        List<TableTop> games = TableTop.selectAll(getApplicationContext());
        for(TableTop game : games){
            itemname.add(game.getName());
            imgId.add(game.getImage());
            itemdescription.add(game.getDescription());
        }

        custom_list_adapter adapter = new custom_list_adapter(this, itemname, imgId, itemdescription);
        list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                String selectedItem = itemname.get(position);
                TableTop selected = TableTop.select(getApplicationContext(), DataBaseContract.TableTop.Name + " = '" + selectedItem + "'");
                Bundle bundle = new Bundle();
                DescriptionFragment fragment = new DescriptionFragment();
                bundle.putParcelable("game", selected);
                fragment.setArguments(bundle);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.game_details, fragment);
                ft.commit();
            }
        });

    }
}
