package cr.ac.ucr.cql.miexamen01;
import android.content.Context;
import android.provider.BaseColumns;

import java.security.acl.LastOwnerException;

public final class DataBaseContract {
    private DataBaseContract () {}

    private static final String COMMA = ", ";
    private static final String TEXT_TYPE = " TEXT ";
    private static final String INTEGER_TYPE = " INTEGER ";
    private static final String REAL_TYPE = " REAL ";
    private static final String PK = " PRIMARY KEY ";
    private static final String FK = " FOREIGN KEY ";
    private static final String REF = " REFERENCES ";
    private static final String NULLABLE = " null ";

    public static class TableTop{

        public static final String TABLE_NAME = "TableTop";
        //Atributos
        public static final String ID = "ID";
        public static final String Name = "Name";
        public static final String Year = "Year";
        public static final String Publisher = "Publisher";
        public static final String Country = "Country";
        public static final String Latitude = "Latitude";
        public static final String Longitude = "Longitude";
        public static final String Description = "Description";
        public static final String NoPlayers = "NoPlayers";
        public static final String Ages = "Ages";
        public static final String PlayingTime = "PlayingTime";
        public static final String Image = "Image"; //Imagen asociada

        //Create
        public static final String SQL_CREATE_TABLETOP =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        ID + TEXT_TYPE + PK + COMMA +
                        Name + TEXT_TYPE + COMMA +
                        Year + INTEGER_TYPE + COMMA +
                        Publisher + TEXT_TYPE + COMMA +
                        Country + TEXT_TYPE + COMMA +
                        Latitude + REAL_TYPE + COMMA +
                        Longitude + REAL_TYPE + COMMA +
                        Description +TEXT_TYPE + COMMA +
                        NoPlayers + INTEGER_TYPE + COMMA +
                        Ages + TEXT_TYPE + COMMA +
                        PlayingTime + TEXT_TYPE + COMMA +
                        Image + INTEGER_TYPE + ")";
        //Delete
        public static final String SQL_DELETE_COUNTRY =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    //Se basa en lo usado en el proyecto para mayor facilidad y comprensión. (Team MP)
    public static String[] DeploymentScript = {
            TableTop.SQL_DELETE_COUNTRY,
            TableTop.SQL_CREATE_TABLETOP
    };
}
