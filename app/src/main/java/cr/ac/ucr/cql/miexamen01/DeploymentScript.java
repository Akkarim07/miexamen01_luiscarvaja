package cr.ac.ucr.cql.miexamen01;
import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DeploymentScript {

    public static void Run(Context context){
        clearDatabase(context);
        createTableTop(context);
    }

    public static void createTableTop(Context context){

        DataAccess db = new DataAccess(context);
        List<TableTop> list = new ArrayList<>();

        //Se hace a punta de String[] :D
        String[] ID = {"TT001", "TT002", "TT003", "TT004", "TT005"};
        String[] name = {"Catan", "Monopoly", "Eldritch Horror", "Magic: The Gathering", "Hanabi"};
        int[] year = {1995, 1935, 2013, 1993, 2010};
        String[] publisher = {"Kosmos", "Hasbro", "Final Flight Games", "Hasbro", "Asmodee"};
        String[] country = {"Germany", "United States", "United States", "United States", "France"};
        double[] lat = {48.774538, 41.883736, 45.015417, 41.883736, 48.761629};
        double[] lon = {9.18467, -71.352259, -93.183995, -71.352259, 2.065296};
        String[] description = {"Picture yourself in the era of discoveries:\n" +
                "after a long voyage of great deprivation,\n" +
                "your ships have finally reached the coast of\n" +
                "an uncharted island. Its name shall be Catan!\n" +
                "But you are not the only discoverer. Other\n" +
                "fearless seafarers have also landed on the\n" +
                "shores of Catan: the race to settle the\n" +
                "island has begun!",
                "The thrill of bankrupting an opponent, but it\n" +
                "pays to play nice, because fortunes could\n" +
                "change with the roll of the dice. Experience\n" +
                "the ups and downs by collecting property\n" +
                "colors sets to build houses, and maybe even\n" +
                "upgrading to a hotel!",
                "An ancient evil is stirring. You are part of\n" +
                "a team of unlikely heroes engaged in an\n" +
                "international struggle to stop the gathering\n" +
                "darkness. To do so, you’ll have to defeat\n" +
                "foul monsters, travel to Other Worlds, and\n" +
                "solve obscure mysteries surrounding this\n" +
                "unspeakable horror.",
                "Magic: The Gathering is a collectible and\n" +
                "digital collectible card game created by\n" +
                "Richard Garfield. Each game of Magic\n" +
                "represents a battle between wizards known as\n" +
                "planeswalkers who cast spells, use artifacts,\n" +
                "and summon creatures.",
                "Hanabi—named for the Japanese word for\n" +
                "\"fireworks\"—is a cooperative game in which\n" +
                "players try to create the perfect fireworks\n" +
                "show by placing the cards on the table in the\n" +
                "right order."
        };
        String[] noPlayers = {"3-4", "2-8", "1-8", "2+", "2-5"};
        String[] ages = {"10+", "8+", "14+", "13+", "8+"};
        String[] playingTime = {"1-2 hours", "20-180 minutes", "2-4 hours", "Varies", "25 minutes"};
        int[] image = {R.drawable.catan, R.drawable.monopoly, R.drawable.eldritch, R.drawable.mtg, R.drawable.hanabi};

        for (int i = 0; i < ID.length; i++){
            list.add(new TableTop(ID[i], name[i], year[i], publisher[i], country[i], lat[i], lon[i],
                    description[i], noPlayers[i], ages[i], playingTime[i], image[i]));
        }

        for (TableTop p :list){
            p.insert(context);
        }

        db.close();
        Log.d("TableTop", "Games were inserted in db");
    }

    private static void clearDatabase(Context context){
        DataAccess db = new DataAccess(context);
        db.resetDatabase();
        Log.d("reset", "Database was reset");
    }
}
