package cr.ac.ucr.cql.miexamen01;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;


public class custom_list_adapter  extends ArrayAdapter<String> {

    private final Activity context;
    private final List<String> itemName;
    private final List<Integer> imgId;
   // private final List<String> itemDescription;

    public custom_list_adapter(Activity context, List<String> itemname, List<Integer> imgid,
                               List<String> itemdescription){
        super(context, R.layout.activity_custom_list_adapter, itemname);

        this.context = context;
        this.itemName = itemname;
        this.imgId = imgid;
       // itemDescription = itemdescription;
    }

    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView =  inflater.inflate(R.layout.activity_custom_list_adapter, null,
                true);

        TextView name = (TextView) rowView.findViewById(R.id.name);
        ImageView image = (ImageView) rowView.findViewById(R.id.icon);
       // TextView description = rowView.findViewById(R.id.description);

        name.setText(itemName.get(position));
        image.setImageResource(imgId.get(position));
       // description.setText(itemDescription.get(position));

        return rowView;
    }
}
