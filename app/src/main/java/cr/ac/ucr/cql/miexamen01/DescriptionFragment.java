package cr.ac.ucr.cql.miexamen01;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import android.app.Fragment;

public class DescriptionFragment extends Fragment {

    private TableTop game;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        Bundle bundle = getArguments();
        if(bundle != null){
            game = bundle.getParcelable("game");
        }
        return inflater.inflate(R.layout.fragment_description, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        TextView title = (TextView) view.findViewById(R.id.fragment_name);
        TextView details = (TextView) view.findViewById(R.id.fragment_description);
        ImageView image  = (ImageView) view.findViewById(R.id.fragment_icon);
        ImageView map  = (ImageView) view.findViewById(R.id.fragment_mapIcon);
        TextView provider = (TextView) view.findViewById(R.id.fragment_provider);
        TextView year = (TextView) view.findViewById(R.id.fragment_year);
        TextView ages = (TextView) view.findViewById(R.id.fragment_ages);
        TextView noPlayers = (TextView) view.findViewById(R.id.fragment_noPlayers);

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MapsActivity.class);
                intent.putExtra("lat", game.getLatitude());
                intent.putExtra("lng", game.getLongitude());
                intent.putExtra("name", game.getPublisher());
                startActivity(intent);
            }
        });
        String kiddo = Integer.toString(game.getYear());
        title.setText(game.getName());
        details.setText(game.getId() + " " + game.getDescription());
        year.setText(kiddo);
        provider.setText(game.getPublisher());
        ages.setText(game.getAges());
        noPlayers.setText(game.getNoPlayers());
        image.setImageResource(game.getImage());
    }
}
