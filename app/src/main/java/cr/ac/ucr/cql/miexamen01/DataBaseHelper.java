package cr.ac.ucr.cql.miexamen01;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.ContactsContract;
import androidx.annotation.Nullable;

public class DataBaseHelper  extends SQLiteOpenHelper {

    /**Increment this counter by one whenever the database model is modified.*/
    private static final int VERSION = 1;

    private static final String DB_NAME = "TableTop.db";

    public DataBaseHelper(@Nullable Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for(String statement : DataBaseContract.DeploymentScript){
            db.execSQL(statement.replaceAll("\\s+", " "));
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion){
        onUpgrade(db, oldVersion, newVersion);
    }
}
