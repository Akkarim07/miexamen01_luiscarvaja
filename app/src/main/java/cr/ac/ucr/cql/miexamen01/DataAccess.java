package cr.ac.ucr.cql.miexamen01;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * @class DataAccess Wrapper object around underlying databases.
 * @brief Provides an abstraction level for CRUD operations.
 * All database entities objects should use this class CRUD methods to perform their database operations
 * instead of using DatabaseHelper directly.
 * */
public class DataAccess {

    private SQLiteDatabase database;
    private SQLiteOpenHelper openHelper;
    private static DataAccess instance;

    public static DataAccess getInstance(Context context){
        if(instance == null)
            instance = new DataAccess(context);
        return instance;
    }

    public DataAccess(Context context){
        this.openHelper = new DataBaseHelper(context);
    }

    public void open(){
        this.database = openHelper.getWritableDatabase();
    }

    public void close(){
        if(database != null)
            database.close();
    }

    public Cursor selectAll(String fromTable){
        String query = "SELECT * FROM " + fromTable + ";";
        return database.rawQuery(query, null);
    }

    public Cursor select(String projection, String fromTable, String where){
        if(projection == null)
            projection = "*";
        String query = "SELECT " + projection + " FROM " + fromTable + " WHERE " + where + ";";
        return database.rawQuery(query, null);
    }

    public long insert(String tableName, ContentValues values){
        long result = 0;
        try {
            result = database.insert(tableName, null, values);
        }catch(Exception e){
            Log.d("exception", e.toString());
        }
        Log.d("insert", Long.toString(result));
        return result;
    }

    public void resetDatabase(){
        this.database = openHelper.getWritableDatabase();
        openHelper.onCreate(database);
    }
}
