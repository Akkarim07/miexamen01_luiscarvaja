package cr.ac.ucr.cql.miexamen01;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class TableTop implements Parcelable {

    private String id;
    private String name;
    private int year;
    private String publisher;
    private String country;
    private double latitude;
    private double longitude;
    private String description;
    private String noPlayers;
    private String ages;
    private String playingTime;
    private int image;

    public TableTop(String id, String name, int year, String publisher, String country,
                    double latitude, double longitude, String description, String noPlayers,
                    String ages, String playingTime, int image) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.publisher = publisher;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
        this.description = description;
        this.noPlayers = noPlayers;
        this.ages = ages;
        this.playingTime = playingTime;
        this.image = image;
    }

    public TableTop(){};

    protected TableTop(Parcel in) {
        id = in.readString();
        name = in.readString();
        year = in.readInt();
        publisher = in.readString();
        country = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        description = in.readString();
        noPlayers = in.readString();
        ages = in.readString();
        playingTime = in.readString();
        image = in.readInt();
    }

    public static final Creator<TableTop> CREATOR = new Creator<TableTop>() {
        @Override
        public TableTop createFromParcel(Parcel in) {
            return new TableTop(in);
        }

        @Override
        public TableTop[] newArray(int size) {
            return new TableTop[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeInt(year);
        dest.writeString(publisher);
        dest.writeString(country);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(description);
        dest.writeString(noPlayers);
        dest.writeString(ages);
        dest.writeString(playingTime);
        dest.writeInt(image);
    }

    public String getId() { return id; }

    public String getName() { return name; }

    public int getYear() { return year; }

    public String getPublisher() { return publisher; }

    public String getCountry() { return country; }

    public double getLatitude() { return latitude; }

    public double getLongitude() { return longitude; }

    public String getDescription() { return description; }

    public String getNoPlayers() { return noPlayers; }

    public String getAges() { return ages; }

    public String getPlayingTime() { return playingTime; }

    public int getImage() { return image; }

    public void setId(String id) { this.id = id; }

    public void setName(String name) { this.name = name; }

    public void setYear(int year) { this.year = year; }

    public void setPublisher(String publisher) { this.publisher = publisher; }

    public void setCountry(String country) { this.country = country; }

    public void setLatitude(double latitude) { this.latitude = latitude; }

    public void setLongitude(double longitude) { this.longitude = longitude; }

    public void setDescription(String description) { this.description = description; }

    public void setNoPlayers(String noPlayers) { this.noPlayers = noPlayers; }

    public void setAges(String ages) { this.ages = ages; }

    public void setPlayingTime(String playingTime) { this.playingTime = playingTime; }

    public void setImage(int image) { this.image = image; }



    public long insert(Context context){
        ContentValues values = new ContentValues();
        values.put(DataBaseContract.TableTop.ID, getId());
        values.put(DataBaseContract.TableTop.Name, getName());
        values.put(DataBaseContract.TableTop.Year, getYear());
        values.put(DataBaseContract.TableTop.Publisher, getPublisher());
        values.put(DataBaseContract.TableTop.Country, getCountry());
        values.put(DataBaseContract.TableTop.Latitude, getLatitude());
        values.put(DataBaseContract.TableTop.Longitude, getLongitude());
        values.put(DataBaseContract.TableTop.Description, getDescription());
        values.put(DataBaseContract.TableTop.NoPlayers, getNoPlayers());
        values.put(DataBaseContract.TableTop.Ages, getAges());
        values.put(DataBaseContract.TableTop.PlayingTime, getPlayingTime());


        DataAccess dataAccess = DataAccess.getInstance(context);
        dataAccess.open();
        long result = dataAccess.insert(DataBaseContract.TableTop.TABLE_NAME, values);
        dataAccess.close();
        return result;
    }

    public static List<TableTop> selectAll(Context context){
        DataAccess dataAccess = DataAccess.getInstance(context);
        dataAccess.open();

        Cursor cursor = dataAccess.selectAll(
                DataBaseContract.TableTop.TABLE_NAME
        );
        cursor.moveToFirst();
        List<TableTop> games = new ArrayList<>();

        while (!cursor.isAfterLast()){
            games.add(new TableTop(
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.ID)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Name)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Year)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Publisher)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Country)),
                    cursor.getDouble(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Latitude)),
                    cursor.getDouble(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Longitude)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Description)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.NoPlayers)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Ages)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.PlayingTime)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Image))
            ));
            cursor.moveToNext();
        }
        cursor.close();
        dataAccess.close();
        Log.d("gamesRead", "All games were read");
        return games;
    }

    public static TableTop select(Context context, String where){
        DataAccess dataAccess = DataAccess.getInstance(context);
        dataAccess.open();

        Cursor cursor = dataAccess.select(
                null,
                DataBaseContract.TableTop.TABLE_NAME,
                where
        );
        cursor.moveToFirst();

        TableTop games = new TableTop(
                cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.ID)),
                cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Name)),
                cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Year)),
                cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Publisher)),
                cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Country)),
                cursor.getDouble(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Latitude)),
                cursor.getDouble(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Longitude)),
                cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Description)),
                cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.NoPlayers)),
                cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Ages)),
                cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.PlayingTime)),
                cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseContract.TableTop.Image))
        );
        cursor.close();
        dataAccess.close();
        return games;
    }
}
